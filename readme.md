# Kafka consumer

Suppose there is record in etc/hosts which maps ip address of virtual machine where runs docker (kafka, manager, ...) to name **kafka01**. 
E.g.:
```
192.168.56.101 kafka01
```
The DNS name `kafka01` is used in this project config.